<?php

namespace Drupal\rest_register_verify_email\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ResendController.
 */
class ResendController extends ControllerBase {

  protected $messenger;

  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  public function resend($user, Request $request) {
    // Check if account is unverified.
    if (empty($user->getLastAccessedTime())) {
      // Resend verify account token mail.
      _rest_register_verify_email_user_mail_notify('email_verify_register_rest', $user);
      $this->messenger->addStatus('Account verification email was sent to ' . $user->getAccountName() . '.');
    } else {
      $this->messenger->addError('This account has already been verfied.');
    }
    $previousUrl = $request->headers->get('referer');
    $response = new RedirectResponse($previousUrl);
    return $response;
  }
}
